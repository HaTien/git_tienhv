<?php
	include("vendor/autoload.php");
	require_once('connect.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Register page</title>
	<link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/assets/css/docs.min.css">
	<link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.css">
	<link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.min.css">
</head>
<body>
	<?php 
        $error = array();
        function validateEmail($email)
        {
            return (!filter_var($email, FILTER_VALIDATE_EMAIL)) ? FALSE : TRUE;
        }

        function validatePassword($password)
        {
         return (strlen($password) < 6 || strlen($password) > 50);
        }

        if ( isset($_POST['register'])) {
            $email = isset( $_POST['email_address']) ? $_POST['email_address'] : " ";
            $password = isset( $_POST['password']) ? $_POST['password'] : " ";
            $confirm_password = isset( $_POST['confirm_password']) ? $_POST['confirm_password'] : " ";
            if ( empty($email)) {
                $error['email_address'] = "Xin mời nhập Email";      
            } else if (!validateEmail($email)) {
                $error['email_address'] = "Email không đúng định dạng"; 
            }
            if ( empty($password)) {
                $error['password'] = "Xin mời nhập Password";
            } else if (validatePassword($password)) {
                $error['password'] = "Password không đúng định dạng";
            }
            if ( empty($confirm_password)) {
                $error['confirm_password'] = " Xin mời bạn nhập lại Password ";
            } else if ($password != $confirm_password) {
                $error['confirm_password'] = " Password bạn vừa nhập không trùng khớp";
            }
            if (empty($error)) {
                $insertUser = $conn->prepare("INSERT INTO users (mail_address,password) VALUE (:email,:password)");
                $insertUser->bindParam(':email', $email);
                $insertUser->bindParam(':password', $password);
                $insertUser -> execute();
            }
        }
    ?>
    <form action="" method="POST">
        <div class="container">
            <h1 style="text-align: center;">Create account</h1>
            <div class="form-group">
                <label>Mail address</label>
                <input type="email" name="email_address" id="email_address" class="form-control" value="<?php echo isset($email_address)? $email_address:"" ?>" placeholder="Email">
                <?php echo isset($error['email_address']) ? $error['email_address'] : " "; ?>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" id="password" class="form-control" value="<?php echo isset($password)? $password:"" ?>" placeholder="Password">
                <?php echo isset( $error['password']) ? $error['password'] : " "; ?>
            </div>
            <div class="form-group">
                <label>Confirm password</label>
                <input type="password" name="confirm_password" id="confirm_password" class="form-control" value="<?php echo isset($confirm_password)? $confirm_password:"" ?>" placeholder="Password">
                <?php echo isset( $error['confirm_password']) ? $error['confirm_password'] : " "; ?>
            </div>
            <div class="form-group">
                <input type="submit" name="register" id="register" value="Register" class="btn btn-primary">
           </div>
        </div>    
    </form>
</body>
</html>